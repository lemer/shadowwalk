import PlaygroundSupport
import SpriteKit

/*:
 # 🌗 ShadowWalk
 *by Olivier Lemer*
 
 Hi there!
 
 This playground is a mini-game that revolves around one main idea: letting shadow edges be physical surfaces.
 Have you ever played as a kid, on a sunny day, a game in which you could only walk on floor that was covered with shadows? I know I did, and this is where my idea came from for this playground.
 
 You play this black sphere, and you have to reach the colored circle. A bright light is following you around and casting sharp shadows on the background.\
 While standing on the floor, you can switch to "the Upside Down" into the shadows, by pressing space. From that point on, the light (lost without you! 😢) will stay put, and you will be able to walk on the edges of the shadows it is casting. (Maybe that's not very clear but it will make sense when you play it!)
 
 ## 🎹 - Sound
 Notice how the music changes when hiding in the shadows! I used Garageband to create two separate tracks that dynamically adapt to your state.
 
 ## ⌘ - Skip a level
 I know you probably don't have much time, so if you get stuck at any point, pressing `s` will `s`kip to the next level, and `r` will `r`estart the current level.
 
 *Enough talking! The thing is probably done loading already; have fun! 🙃*
 
 
 ### More about...

 ## 🔧 - Level Creation
 All levels are stored in the "Levels" folder. You can go check out TemplateLevel.sks if you want a starting point to create you own levels. You'll find all building blocks there along with many more details. When you are done, make sure to rename the file to something like "Level10.sks".
 
 ## ⌨️ - The Code
 This playground is organized into multiple classes. If you have time, I think the most interesting magic happens in ShadowScene's `generatePhysics()` method. This is where intersections between blocks are created, rays are traced, and visible points are selected to generate the surfaces you walk on when in the shadows. There are a lot more details there!
 
 Here is an overview of the overall organization of most classes (of course, each file and class contains much more detailed information):
 - The main class is `ShadowScene`. It is responsible for managing keyboard inputs, physics collisions, and most importantly, switching between light and dark mode. This switching process is probably the most complex part of the project. It also makes use of the `ShadowPoint` class, which abstracts the concept of the visible part of a platform.
 - In `ShadowScene.swift`, you will also find two other important classes:
    - `ShadowController` keeps track of the game's shadow mode (whether dark, light, or in-between),
    - `ShadowWatcher` is a protocol that observes changes in the controller's shadow mode. Any class that implements this protocol can start watching a controller, and will receive callbacks whenever its mode gets updated. This allows for very simple and elegant management of shadow mode, in the sense that each object in the game is the only one deciding what to do upon a shadow change, rather than asking the controller or the scene to do everything. It compartmentalizes the task, making it much more managable.
 - While the other classes are pretty straightforward, there still is some interesting things to see in each one of them: how levels are extracted from nodes — to allow the use of XCode's Scene Editor to create levels; the animations and movements of the player; the syncronization of the music in light and dark mode...
 - Finally the file Utils.swift groups many helpers for the playground, such as useful extensions to `CGFloat` or `CGPoint`, a `PhysicsCategory` class simplifying masking, or the `Performances` class I used to improve the efficiency of this playground.

 */

// Load the SKScene from 'GameScene.sks'
let sceneView = SKView(frame: CGRect(x:0 , y:0, width: 640, height: 480))

// Create the scene
let scene = ShadowScene(size: CGSize(width: 1024, height: 768))
scene.scaleMode = .aspectFill

// You can force the scene's next level to skip to any level you want! :)
scene.nextLevel = 0

// Present the scene
sceneView.presentScene(scene)
sceneView.showsPhysics = false

PlaygroundSupport.PlaygroundPage.current.liveView = sceneView

