import Foundation
import AVFoundation
import SpriteKit

/**
 Groups and simplifies all audio-related actions, namely music management and
 sound effect.
 The initializer for this class expects to find specific files in the
 Audio/Sound subfolder.
 */
public class ShadowAudio: ShadowWatcher {
    // Music players
    private let corePlayer: AVAudioPlayer
    private let darkPlayer: AVAudioPlayer
    
    // Sound 'players'
    private let hitAudioNode: SKAudioNode
    private let switchAudioNode: SKAudioNode
    
    // Whether Music should be played or not. Does not affect sound effects
    private let shht = false

    public init?() {
        do {
            self.corePlayer = try ShadowAudio.loadAudioPlayer(
                named: "musicCore")
            self.darkPlayer = try ShadowAudio.loadAudioPlayer(
                named: "musicDark")
            self.hitAudioNode = try ShadowAudio.loadAudioNode(
                named: "hit")
            self.switchAudioNode = try ShadowAudio.loadAudioNode(
                named: "switch")
        } catch {
            return nil
        }
        self.darkPlayer.volume = 0
    }
    
    /// Returns an AVAudioPlayer from the provided mp3 file
    private static func loadAudioPlayer(named name: String) throws
        -> AVAudioPlayer {
        guard
            let path = Bundle.main.path(
                forResource: name, ofType: "mp3",
                inDirectory: "Audio/Music"),
            let player = try? AVAudioPlayer(
                contentsOf: URL(fileURLWithPath: path))
            else {
                print("Impossible to find or load music file \(name).mp3")
                throw AudioError.fileNotFound
        }
        
        player.numberOfLoops = -1
        player.prepareToPlay()
        
        return player
    }
    
    /// Returns a SKAudioNode from the provided mp3 file
    private static func loadAudioNode(named name: String) throws
        -> SKAudioNode {
        guard
            let path = Bundle.main.path(
                forResource: name, ofType: "mp3",
                inDirectory: "Audio/Sound")
            else {
                print("Impossible to find or load sound file \(name).mp3")
                throw AudioError.fileNotFound
        }
        
        let audioNode = SKAudioNode(url: URL(fileURLWithPath: path))
        audioNode.autoplayLooped = false
        audioNode.run(.changeVolume(to: 0.1, duration: 0))
        
        return audioNode
    }
    
    /// Attaches sound effects to the provided node
    public func attachPlayerAudio(to node: SKNode) {
        self.switchAudioNode.removeFromParent()
        self.hitAudioNode.removeFromParent()

        node.addChild(self.switchAudioNode)
        node.addChild(self.hitAudioNode)
    }
    
    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        switch (src, dst) {
        case (_, .soonDark), (.light, .dark):
            playSwitch()
            includeDarkMusic()
        case (_, .soonLight), (.dark, .light):
            removeDarkMusic()
        default: ()
        }
    }
    
    /// Plays the sound of the playing switching shadow modes
    public func playSwitch() {
        self.switchAudioNode.run(.sequence([.stop(), .play()]))
    }

    /// Plays the sound of the player hitting a wall or surface
    public func playHit() {
        self.hitAudioNode.run(.sequence([.stop(), .play()]))
    }

    /// Starts playing music, if not playing already.
    public func play() {
        if shht {
            return
        }
        if !self.corePlayer.isPlaying {
            self.corePlayer.volume = 0
            self.corePlayer.play()
            self.corePlayer.setVolume(0.5, fadeDuration: 3)
        }
    }
    
    /// Lets dark musing join in
    private func includeDarkMusic() {
        if shht {
            return
        }
        let offset: TimeInterval = 0.75
        let currentTime = self.corePlayer.currentTime
        let deviceCurrentTime = self.corePlayer.deviceCurrentTime
        
        self.darkPlayer.stop()
        self.darkPlayer.currentTime = currentTime + offset
        self.darkPlayer.play(atTime: deviceCurrentTime + offset)
        self.darkPlayer.setVolume(0.5, fadeDuration: 2)
    }
    
    /// Removes dark music from the mix
    private func removeDarkMusic() {
        self.darkPlayer.setVolume(0, fadeDuration: 3)
    }
}

/// Represents the error occuring when an audio file could not be read
fileprivate enum AudioError: Error {
    case fileNotFound
}
