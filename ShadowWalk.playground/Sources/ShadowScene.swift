import Foundation
import SpriteKit

/**
 This class represents the main scene in which the game is initialized and
 played. It is responsible for managing keyboard inputs, physics collisions
 and switching between light and dark modes.
 */
public class ShadowScene: SKScene, SKPhysicsContactDelegate, ShadowWatcher {
    // Levels
    private var levels: [ShadowLevel] = []
    public var nextLevel: Int = 0
    public let loopBackLevel: Int = 8
    private var currentLevel: ShadowLevel? = nil
    
    // Audio
    private var audio: ShadowAudio!
    
    // Player
    private let playerRadius: CGFloat = 13
    private var player : ShadowPlayer!
    private var respawnPos : CGPoint!
    private var respawning = false

    // Light
    private var lightNode : ShadowLight!

    // Shadow Controller
    public let shadowController = ShadowController()

    // Container for physicsbody generated in dark mode
    private var physicsNode : SKNode!
    
    // Introduction slide with controllers
    private var controlsSprite: SKSpriteNode!
    private var waitsToPlay: Bool = true
    private var ignoreInput: Bool = false
    
    public static var singleton: ShadowScene?

    public override init(size: CGSize) {
        super.init(size: size)
        ShadowScene.singleton = self
        self.startWatching(controller: self.shadowController)
        self.extractLevels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Finds all level files in the Levels directory of the form Level<i>.sks
     Starts by looking for a Level0.sks file, and then looks for more levels
     with consecutive ids.
     */ 
    private func extractLevels() {
        var idx = 0
        let filePrefix = "Levels/Level"
        var fileName = filePrefix + String(idx)
        
        while
            Bundle.main.path(forResource: fileName, ofType: "sks") != nil,
            let ref = SKReferenceNode(fileNamed: fileName),
            ref.children.count == 1 {
                
                ref.resolve()
                let node = ref.children[0]
                guard let shadowLevel = ShadowLevel(from: node) else {
                    print("Could not extract level from reference; ignoring it")
                    continue
                }
                self.levels.append(shadowLevel)
                idx += 1
                fileName = filePrefix + String(idx)
        }
    }
    
    override public func didMove(to view: SKView) {
        // Audio
        self.audio = ShadowAudio()
        if self.audio == nil {
            fatalError()
        }
        self.audio.startWatching(controller: self.shadowController)
        
        // Camera
        let camera = SKCameraNode()
        self.camera = camera
        self.addChild(camera)

        // Collisions setup        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -9)
        self.physicsWorld.contactDelegate = self

        self.physicsNode = SKNode()
        self.addChild(self.physicsNode)
        
        // Player and light setup
        self.player = ShadowPlayer(radius: playerRadius, color: .black)
        self.audio.attachPlayerAudio(to: self.player)
        self.player.startWatching(controller: self.shadowController)
        
        self.lightNode = ShadowLight()
        self.lightNode.setTarget(self.player)
        self.lightNode.startWatching(controller: self.shadowController)
        
        let background = SKSpriteNode(color: .gray, size: self.size)
        self.addChild(background)
        background.shadowCastBitMask = ShadowLight.lightCategory
        background.lightingBitMask = ShadowLight.lightCategory

        // Creating killer walls outside the screen
        self.setupKillerWalls(margin: playerRadius * 2)

        if !self.loadControlsSlide() {
            self.loadGame()
        }
    }
    
    /// Loads the introductory tutorial-y controls screen
    private func loadControlsSlide() -> Bool {
        guard let path = Bundle.main.path(forResource: "controls",
                                          ofType: "png",
                                          inDirectory: "Images")
            else {
                print("Hmm, everything's not going as planed; "
                    + "the introductory controls screen wasn't found. "
                    + "We'll keep going though!")
                return false
        }

        self.controlsSprite = SKSpriteNode(imageNamed: path)
        if (self.controlsSprite == nil) {
            print("SKSpriteNode could not load controls image. "
                + "We'll have to do without it!")
            return false
        }
        self.controlsSprite.size = self.size
        self.controlsSprite.zPosition = ZLayer.ui
        self.addChild(self.controlsSprite)
        return true
    }
    
    /// Loads the actual game
    private func loadGame() {
        self.audio.play()
        
        self.addChild(self.player)
        self.addChild(self.lightNode)
        
        self.presentNextLevel()
        self.resetPlayer()
        self.lightNode.setColor(self.currentLevel?.color ?? .green)
        
        self.controlsSprite?.run(.sequence([.fadeOut(withDuration: 1),
                                            .removeFromParent()]))
    }
    
    /// Sets up walls above and under the screen that will kill the player
    /// if it exits the screen
    private func setupKillerWalls(margin: CGFloat) {
        let thickness: CGFloat = 10
        let horizontalPhysics = SKPhysicsBody(
            rectangleOf: CGSize(width: self.size.width * 2,
                                height: thickness),
            center: CGPoint(x: 0,
                            y: self.size.height / 2.0 + margin + thickness / 2))
        
        let node = SKNode()
        node.physicsBody = horizontalPhysics

        // setting up masks
        node.physicsBody!.categoryBitMask = PhysicsCategory.sensor.mask
        node.physicsBody!.collisionBitMask = 0
        node.physicsBody!.contactTestBitMask = PhysicsCategory.player.mask
        node.physicsBody!.affectedByGravity = false
        
        let bottomNode = node.copy() as! SKNode
        bottomNode.zRotation = CGFloat.pi
        bottomNode.position = CGPoint(x: 0, y: -150)
        
        self.addChild(node)
        self.addChild(bottomNode)
    }

    /// Gets the next level in the queue and sets it up. If there are no more
    /// levels to present, loops back to loopBackLevel
    private func presentNextLevel() {
        if self.nextLevel < 0 {
            self.nextLevel = 0
        }
        var resetPlayerPos = false
        if self.nextLevel >= self.levels.count {
            if self.loopBackLevel >= self.levels.count {
                print("No more levels to play! Thank you for playing!! :)")
                exit(0)
            }
            self.nextLevel = self.loopBackLevel
            resetPlayerPos = true
        }
        
        let nextLevel = self.levels[self.nextLevel]
        self.nextLevel += 1
        
        if nextLevel.teleportOnLoad {
            resetPlayerPos = true
        }

        nextLevel.startWatching(controller: self.shadowController)
        
        nextLevel.alpha = 0.01
        nextLevel.run(.fadeIn(withDuration: 1)) {
            nextLevel.goal.isTriggerable = true
        }
        
        currentLevel?.removeFromParent()
        
        self.currentLevel = nextLevel
        self.addChild(nextLevel)
        
        if resetPlayerPos {
            self.resetPlayer()
        }
        
        self.respawnPos = self.currentLevel?.playerPosition
    }
    
    /// Triggers the animation for switching levels, and switches to next level.
    private func completeLevel() {
        self.player.physicsBody?.velocity = CGVector.zero
        self.player.physicsBody?.isDynamic = false
        
        self.ignoreInput = true
        
        self.currentLevel?.run(.fadeOut(withDuration: 1))
        self.lightNode.flash(andThen: {
            self.presentNextLevel()
            self.lightNode.setTarget(self.player)
            self.lightNode.flashBack(
                withColor: self.currentLevel?.color ?? .white)
            self.player.physicsBody?.isDynamic = true
            self.ignoreInput = false
        })
    }

    /**
     This function is called upon switching from light to dark mode. It works
     in the following three main stages:
     
     - Listing all visible points: each platform has either 2 or 3 visible
     points (where we define visibility of a point with respect to the
     corresponding platform only, regardless of other platforms). We build
     the union of all those points and sort them in increasing order of angle
     in spherical coordinate.
     - Inserting platform intersections: given all visible points, we look for
     visible intersections of platform edges, and add them to the set of
     visible points. In other words, if two surfaces of two different platforms
     intersect, and if those two surfaces are both visible, then we add the
     intersection point to the list of visible points.
     - Inserting ray intersections and filtering out hidden points: for each
     visible point (including platform intersections), we trace a ray from the
     light source to that point and look for intersections with other platforms.
     If such intersections are closer to the light than the considered point,
     then that point is hidden and must not be added. Otherwise, it is added
     along with the ray intersection we found, if the considered point is the
     first or last visible point of the corresponding platform.
     
     Note that this method's code is simplified greatly by the use of the
     ShadowPoint class, which groups all necessary information about a
     platform's visible points and the lines leaving them. Many more details are
     explained in ShadowPoint.swift.
     In particular, we say that a point A is a source of a destination point B
     if A-B forms an edge in the platform that A and B originated from.
     
     */
    private func generatePhysics() {
        guard let currentLevel = self.currentLevel else {
            return
        }
        
        /**
         Helper function. Pre-fills the srcs set -- a set of all points that
         could be potential sources of points of the first quadrant (0 to pi/2).
         For each point in the last quadrant (3pi/2 to 2pi), add it to the set
         of source points, and remove any already found source point that this
         point is a destination of, such that the final set contains exactly
         all points in the last quadrant that are sources but have no
         destinations yet.
         */
        func getStartingSrcs(_ visiblePoints: [ShadowPoint])
            -> Set<ShadowPoint> {
                var srcs = Set<ShadowPoint>()
                // for each point p in the last quadrant, add it to the set of
                // sources, and remove any already found source point that was a 
                // source of p
                for pt in visiblePoints.filter(
                    { $0.spheric.phi >= CGFloat.pi * 1.5 }) {
                        if pt.order == .intersection {
                            continue
                        }
                        if let src = srcs.first(
                            where: { $0.isSrcOf(dst: pt.point) }) {
                            srcs.remove(src)
                        }
                        srcs.insert(pt)
                }
                return srcs
        }
        
        // Getting all visible points and sorting them
        var visiblePoints: [ShadowPoint] = []
        for platform in currentLevel.shadowPlatforms {
            guard let pts = platform.visiblePoints(
                from: self.lightNode.position)
                else {
                    print("Platform has no visible points; it is not lit yet.")
                    continue
            }
            visiblePoints.append(contentsOf: pts)
        }

        visiblePoints.sort { (a, b) -> Bool in
            a.spheric.phi < b.spheric.phi
        }

        // Maps all points to their source point, if any. This avoids having to
        // recompute sources of all points more than once
        var srcForPoint: [ShadowPoint: ShadowPoint] = [:]

        // Inserting Platform intersections to visible points
        var srcs = getStartingSrcs(visiblePoints)
        var intersections: [ShadowPoint] = []
        for pt in visiblePoints {
            // if there exists a source for this point
            if let src = srcs.first(where: { $0.isSrcOf(dst: pt.point) }) {
                // memorize it
                srcForPoint[pt] = src
                srcs.remove(src)
                if let inter = ShadowPoint.findClosestIntersectingPoint(
                    in: srcs, between: src.point, and: pt.point)
                {
                    // if any source point forms a line that intersects this
                    // line between src and pt, we've got a platform
                    // intersection!
                    let sph = inter.toSpheric()
                    let sp = ShadowPoint(point: inter,
                                         spheric: sph,
                                         nextPoint: nil,
                                         order: .intersection)
                    intersections.append(sp)
                }
            }
            if pt.order != .last {
                srcs.insert(pt)
            }
        }

        visiblePoints.append(contentsOf: intersections)
        visiblePoints.sort(by: { $0.spheric.phi < $1.spheric.phi })
        
        // Inserting ray intersections and filtering out hidden points
        srcs = getStartingSrcs(visiblePoints)
        var points = [CGPoint]()
        for pt in visiblePoints {
            // If this point has a source, we remove it so that it does not
            // affect the finding of intersecting points
            if let src = srcForPoint[pt] {
                srcs.remove(src)
            }
            // We trace a ray to the point and see if any line intersects it
            if let inter = ShadowPoint.findClosestIntersectingPoint(
                in: srcs, between: CGPoint.zero, and: pt.point, infinite: true)
            {
                // If such a line is farther than the point, we can add it.
                // We also add the intersection point if pt is first or last
                if (inter.toSpheric().rho
                    >= pt.spheric.rho - CGFloat.floatingPrecision)
                {
                    if pt.order == .first {
                        points.append(inter)
                    }
                    points.append(pt.point)
                    if pt.order == .last {
                        points.append(inter)
                    }
                }
            } else {
                // If there is no intersecting line, then we can add pt too
                points.append(pt.point)
            }
            // Finally, if pt had an src
            if let src = srcForPoint[pt] {
                // If pt is an intersection, we put src back,
                // otherwise we insert pt instead, such that it can be a source
                // for future points
                if pt.order == .intersection {
                    srcs.insert(src)
                } else {
                    srcs.insert(pt)
                }
            }
            // If pt was first we add it as a source too
            if pt.order == .first {
                srcs.insert(pt)
            }
            
        }
        
        // points now holds the points forming a closed path around the light.
        if let path = CGPath.from(points: points) {
            self.physicsNode.physicsBody = SKPhysicsBody(polygonFrom: path)
            self.physicsNode.physicsBody?.isDynamic = false
            self.physicsNode.physicsBody?.collisionBitMask =
                PhysicsCategory.player.mask
            self.physicsNode.physicsBody?.categoryBitMask =
                PhysicsCategory.platform.mask
            self.physicsNode.position = self.lightNode.position
        } else {
            print("CGPath had trouble creating path from points")
        }
    }
    
    /// Teleports the player back to its starting position
    private func resetPlayer() {
        self.player.position =
            self.currentLevel?.playerPosition ?? CGPoint.zero
        self.player.physicsBody?.velocity = CGVector.zero
        self.respawnPos = self.player.position
        self.teleportLight()
    }
    
    /// Teleports the light to the player
    private func teleportLight() {
        if (self.shadowController.mode == .light) {
            self.lightNode.teleport(to: self.player.position)
        }
    }
    
    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        if dst == .light {
            self.physicsNode.physicsBody = nil
        }
        switch dst {
        case .soonLight:
            self.lightNode.position =
                self.player.position + CGPoint(x: 0, y: 2 * self.player.radius)
            fallthrough
        case .soonDark:
            self.camera?.run(.sequence([.scale(to: 0.98, duration: 0.1),
                                        .scale(to: 1, duration: 0.1)]))
        case .dark:
            self.generatePhysics()
            fallthrough
        case .light:
            self.physicsWorld.gravity =
                CGVector(dx: 0, dy: -self.physicsWorld.gravity.dy)
            
            self.respawnPos = self.player.position
        }        
    }
    
    override public func keyDown(with event: NSEvent) {
        if self.ignoreInput {
            return
        }
        // Movement
        let code = Int(event.keyCode)
        switch code {
        case 126: // up
            self.player.move(towards: .N)
        case 125: // down
            self.player.move(towards: .S)
        case 124: // right
            self.player.move(towards: .E)
        case 123: // left
            self.player.move(towards: .W)
        case 49: // space
            if self.player.canSwitch {
                self.shadowController.switchToNext()
            } else {
                self.camera?.run(.repeat(
                    .sequence([.scale(to: 0.98, duration: 0.1),
                               .scale(to: 1, duration: 0.1)]),
                    count: 2)
                )
            }
        default: ()
        }
        
        // 'r' and 's' shortcuts
        if (self.shadowController.mode == .dark
            || self.shadowController.mode == .light)
            && !self.respawning
        {
            if event.characters?.contains("s") ?? false {
                // if necessary, switch back to light mode
                if self.shadowController.mode == .dark {
                    self.shadowController.mode = .light
                }
                self.player.run(.move(to: self.currentLevel!.goal.position,
                                      duration: 0))
                self.lightNode.teleport(to: self.currentLevel!.goal.position)
                return
            }
            
            if event.characters?.contains("r") ?? false {
                if self.shadowController.mode == .dark {
                    self.shadowController.mode = .light
                }
                self.resetPlayer()
            }
        }
    }
    
    override public func keyUp(with event: NSEvent) {
        let code = Int(event.keyCode)
        // We need to wait a little before registering keyUp.
        // This is due to what looks like a bug that calls keyDown
        // one last time, even after keyUp was called
        self.run(.sequence([
            .wait(forDuration: 0.06),
            .run {
                switch code {
                case 126, 125:
                    self.player.stopMoving(along: .N)
                case 124, 123:
                    self.player.stopMoving(along: .E)
                default:
                    break
                }
            }]))
    }
    
    public override func mouseDown(with event: NSEvent) {
        if self.waitsToPlay {
            self.waitsToPlay = false
            self.loadGame()
            self.controlsSprite = nil
        }
    }
    
    override public func update(_ currentTime: TimeInterval) {
        self.player.update()
        self.lightNode.move()
    }
    
    public func didBegin(_ contact: SKPhysicsContact) {
        // If hit a surface, play a sound
        if (contact.isBetween(PhysicsCategory.anyOf(.platform, .surface),
                              and: PhysicsCategory.player.mask)) {
            self.audio.playHit()
        }
        
        // If touch the goal, complete level
        if (contact.isBetween(PhysicsCategory.goalProximity.mask,
                              and: PhysicsCategory.playerProximity.mask)) {
            if let currentLevel = self.currentLevel,
                self.shadowController.mode == .light,
                self.lightNode.canMove
            {
                currentLevel.goal.isTriggerable = false
                self.lightNode.setTarget(currentLevel.goal)
                self.completeLevel()
            }
        }
        
        // If touches something deadly, respawn
        if ((contact.isBetween(PhysicsCategory.sensor.mask,
                               and: PhysicsCategory.player.mask))
            || (contact.isBetween(PhysicsCategory.killer.mask,
                                  and: PhysicsCategory.player.mask))) {
            self.respawning = true
            self.player.respawn(at: self.respawnPos,
                                andThen: {
                                    self.respawning = false
                                    self.teleportLight()
                                    
            })
        }
    }
    
    @objc public static override var supportsSecureCoding: Bool {
        // SKNode conforms to NSSecureCoding, so any subclass going
        // through the decoding process must support secure coding
        get {
            return true
        }
    }
}

/**
 Represents which mode the game currently is in. Values starting with "soon"
 indicate that the transition animation is currently playing, and it is
 expected to be followed by the actual mode.
 */
public enum ShadowMode: UInt32 {
    case light
    case soonDark
    case dark
    case soonLight
    
    public func next() -> ShadowMode {
        return ShadowMode(rawValue: (self.rawValue + 1) % 4) ?? .light
    }
}

/**
 Represents an observer of a ShadowController.
 A ShadowWatcher can start watching a controller, which will then be notifying
 the watcher of any update of its shadow mode.
 */
public protocol ShadowWatcher {
    func didChangeShadows(from src: ShadowMode, to dst: ShadowMode)
    func startWatching(controller: ShadowController)
}

extension ShadowWatcher {
    public func startWatching(controller: ShadowController) {
        controller.register(self)
    }
}

/**
 A ShadowController keeps track of the current shadow mode.
 It offers the option to be observed by ShadowWatchers, which it will notify
 whenever it changes its shadow mode.

 Notice that a watcher is allowed to change this controller's mode when it gets
 notified. The controller maintains a queue of pending modes; whenever it
 receives a request to update its mode while notifying watchers of an older
 update, it enqueues it and will evaluate it once it is done with the current
 update.
 */
public class ShadowController {
    private var watchers: [ShadowWatcher] = []

    private var notifying = false
    private var pendingMode = [ShadowMode]()

    private var mode_: ShadowMode = .light
    public var mode: ShadowMode {
        get {
            return self.mode_
        }
        set {
            if self.notifying {
                self.pendingMode.append(newValue)
            } else {
                let oldValue = self.mode_
                self.mode_ = newValue
                self.notifying = true
                for watcher in self.watchers {
                    watcher.didChangeShadows(from: oldValue, to: newValue)
                }
                self.notifying = false
                if !self.pendingMode.isEmpty {
                    if self.pendingMode.count > 100 {
                        fatalError("Cyclic updates of shadowController mode")
                    }
                    self.mode = self.pendingMode.removeFirst()
                }
            }
        }
    }
    
    /// Inform the controller to notify this watcher of updates
    public func register(_ watcher: ShadowWatcher) {
        self.watchers.append(watcher)
    }
    
    /// Requests to switch to next mode
    public func switchToNext() {
        self.mode = self.mode.next()
    }
}
