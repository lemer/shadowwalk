# ShadowWalk

ShadowWalk showcases a fun new game mechanic for 2D platforming games. This was a submission to Apple's [WWDC19 Scholarship](https://developer.apple.com/wwdc19/scholarships/) contest and was accepted by Apple.

This playground was developped on macOS Mojave 10.14.3 with XCode 10.1 and should be playable as is. Following are some small previews of the playground's look and mechanics.

![](gifs/sw_death_lvl.gif)
![](gifs/sw_green_lvl.gif)
![](gifs/sw_glass_lvl.gif)
![](gifs/sw_red_lvl.gif)
![](gifs/sw_final_lvl.gif)
