import Foundation
import SpriteKit

/**
 Represents a generic platform in the game. This superclass on its own is a
 platform that casts no shadows and has no top surface when in dark mode.
 It can be set to be a moving platform if the node provided to the
 initializer has the following information in its userData attribute:
   - "delta_x" : distance the platform must move on the x axis
   - "duration_x" : duration in which the platform must move delta_x distance
 Movement along the y axis are defined similarly.
 
 Additionally, the color of this platform will be defined by the name of the
 node provided to the initializer; if its name is "glass", the platform will
 be made brighter.
 */
public class Platform: SKSpriteNode, ShadowWatcher {
    private var blockPhysicsBody: SKPhysicsBody!

    public private(set) var isMoving: Bool = false

    public convenience init(from node: SKSpriteNode) {
        self.init(texture: node.texture, color: node.color, size: node.size)
        
        let grayScale: CGFloat = (node.name == "glass") ? 0.81 : 0.64
        self.color = NSColor(white: grayScale, alpha: 1)
        
        self.position = node.position
        self.anchorPoint = node.anchorPoint
        self.name = node.name
        
        if let delta_x = node.userData?["delta_x"] as? CGFloat,
            let duration_x = node.userData?["duration_x"] as? Double {
            let move_x = self.position.x + delta_x
            let moveAction = SKAction.moveTo(x: move_x, duration: duration_x)
            let moveBackAction = SKAction.moveTo(x: self.position.x,
                                                 duration: duration_x)
            // I had to remove the timing mode, because it caused more problems
            // with action pausing (whether .isPaused or setting speed to 0)
            //moveAction.timingMode = .easeInEaseOut
            //moveBackAction.timingMode = .easeInEaseOut
            self.run(.repeatForever(.sequence([moveAction, moveBackAction])))
            self.isMoving = true
        }
        
        if let delta_y = node.userData?["delta_y"] as? CGFloat,
            let duration_y = node.userData?["duration_y"] as? Double {
            let move_y = self.position.y + delta_y
            let moveAction = SKAction.moveTo(y: move_y, duration: duration_y)
            let moveBackAction = SKAction.moveTo(y: self.position.y,
                                                 duration: duration_y)
            self.run(.repeatForever(.sequence([moveAction, moveBackAction])))
            self.isMoving = true
        }
    }
    
    public override init(texture: SKTexture?, color: NSColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        
        let physics = SKPhysicsBody(rectangleOf: size, center: self.anchorPoint)
        physics.categoryBitMask = PhysicsCategory.platform.mask
        physics.collisionBitMask = PhysicsCategory.anyOf(.light, .player)
        physics.contactTestBitMask = PhysicsCategory.nothing
        
        physics.isDynamic = false
        self.blockPhysicsBody = physics
        self.physicsBody = physics
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        switch dst {
        case .soonDark :
            self.speed = 0.00001
        case .dark:
            self.physicsBody = nil
        case .light:
            self.physicsBody = self.blockPhysicsBody
            self.speed = 1
        default: ()
        }
    }
}

/**
 This class represents a platform that can cast shadows, and that has a top
 surface, visible and tengible in dark mode.
 */
public class ShadowPlatform: Platform {
    private var points: [CGPoint] = []
    
    fileprivate var surface: SKShapeNode?
    fileprivate var surfacePhysicsBody: SKPhysicsBody!

    public override init(texture: SKTexture?, color: NSColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        
        let anchorX = self.anchorPoint.x * size.width
        let anchorY = self.anchorPoint.y * size.height
        
        self.points = [
            CGPoint(x: -anchorX, y: -anchorY),
            CGPoint(x: size.width - anchorX, y: -anchorY),
            CGPoint(x: size.width - anchorX, y: size.height - anchorY),
            CGPoint(x: -anchorX, y: size.height - anchorY)
        ] 
        
        let line = SKShapeNode(rectOf: CGSize(width: size.width, height: 1))
        line.lineWidth = 1
        line.position = CGPoint(x: 0, y: size.height / 2)
        line.strokeColor = .white
        line.zPosition = ZLayer.surfaces
        self.surface = line
        self.addChild(line)
        
        self.surfacePhysicsBody = SKPhysicsBody(
            edgeFrom: CGPoint(x: -anchorX, y: 0),
            to: CGPoint(x: size.width - anchorX, y: 0))
        self.surfacePhysicsBody.isDynamic = false
        self.surfacePhysicsBody.categoryBitMask = PhysicsCategory.surface.mask
        self.surfacePhysicsBody.contactTestBitMask = PhysicsCategory.player.mask
        line.physicsBody = self.surfacePhysicsBody
        
        self.shadowCastBitMask = ShadowLight.lightCategory
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Removes the top surface of the block. It will thus not be tengible 
    /// anymore in dark mode.
    public func removeSurface() {
        self.surface?.removeFromParent()
        self.surface = nil
    }
    
    /**
     Returns the list of all points visible from the provided position, ordered
     in increasing order of angle in spherical coordinates.
     */
    public func visiblePoints(from position: CGPoint) -> [ShadowPoint]? {
        
        // Take all points
        var points: [(SphericPoint, CGPoint, Int)] = []
        var count = 0
        for point in self.points {
            let pt = point + self.position - position
            points.append((pt.toSpheric(), pt, count))
            count += 1
        }
        
        // Sort them
        points.sort { (a, b) -> Bool in
            a.0.phi < b.0.phi
        }
        
        // Special case if the platform overlaps the point where spherical
        // coordinates loop back from 2pi to 0
        if points.last!.0.phi - points.first!.0.phi > CGFloat.pi {
            points.sort { $0.0.rotated(by: CGFloat.pi).phi
                < $1.0.rotated(by: CGFloat.pi).phi }
        }
        var visiblePoints = [points.first!, points.last!]
        if (visiblePoints[0].0.phi * visiblePoints[1].0.phi < 0
            && visiblePoints[0].2 > visiblePoints[1].2) {
            points.swapAt(0, 1)
            points.swapAt(2, 3)
            visiblePoints = [points.first!, points.last!]
        }
        
        // If there are more than two visible points, add the middle point
        if abs(visiblePoints[0].2 - visiblePoints[1].2) == 2 {
            visiblePoints.insert(points[1], at: 1)
            if points[2].0.rho < points[1].0.rho {
                visiblePoints[1] = points[2]
            }
        }
        
        //assert(visiblePoints.count == 2 || visiblePoints.count == 3)
        
        // Mapping each CGPoint to an equivalent ShadowPoint
        var out = [ShadowPoint]()
        count = 0
        for (sph, pt, _) in visiblePoints {
            let order: ShadowPoint.Order
            switch count {
            case 0, 2:
                order = ShadowPoint.Order(rawValue: count)!
            default :
                if visiblePoints.count == 2 {
                    order = .last
                } else {
                    order = .middle
                }
            }
            count += 1
            let nextPt: CGPoint?
            if count < visiblePoints.count {
                nextPt = visiblePoints[count].1
            } else {
                nextPt = nil
            }
            out.append(ShadowPoint(point: pt,
                                   spheric: sph,
                                   nextPoint: nextPt,
                                   order: order))
        }
        return out
    }
}

/**
 Represents a ShadowPlatform that has a red, deadly top surface.
 */
public class KillerPlatform: ShadowPlatform {
    public override init(texture: SKTexture?, color: NSColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)

        self.surfacePhysicsBody.categoryBitMask |= PhysicsCategory.killer.mask
        self.surface?.strokeColor = .red
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/**
 Represents text displayed in a level. It can be set to appear or disappear
 depending on the current shadow mode.
 
 As for Platforms, information about when this label should appear is encoded
 in its userData attribute:
   - "visibleMode" : can be set to "Dark", "Light" or "Both" and controls when
 the label is visible
 - "reappear" : is a boolean controlling whether this label should appear only
 once, or keep reappearing.
 */
public class ShadowLabel: SKLabelNode, ShadowWatcher {
    private var visibleMode: String = "Both"
    private var reappears: Bool = true
    private var hasAppeared: Bool = true
    
    public convenience init(from label: SKLabelNode) {
        self.init()
        self.fontColor = label.fontColor
        self.position = label.position
        self.fontName = label.fontName
        self.fontSize = label.fontSize
        self.text = label.text
        self.zRotation = label.zRotation
        self.horizontalAlignmentMode = label.horizontalAlignmentMode

        if let data = label.userData {
            if let mode = data["visibleMode"] as? String {
                self.visibleMode = mode
            }
            if let reappears = data["reappear"] as? Bool {
                self.reappears = reappears
            }
        }
        
        self.alpha = 0
        if self.visibleMode == "Dark" {
            self.hasAppeared = false
        } else {
            self.run(.fadeIn(withDuration: 1))
        }
    }
    
    override public init() {
        super.init()
        self.zPosition = ZLayer.ui
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        if self.visibleMode == "Both" {
            return
        }
        if (dst == .dark && self.visibleMode == "Dark")
            || (dst == .light && self.visibleMode == "Light") {
            if (self.reappears || !self.hasAppeared) {
                self.run(.fadeIn(withDuration: 1))
                self.hasAppeared = true
            }
        } else {
            self.run(.fadeOut(withDuration: 1))
        }
    }
    
}
