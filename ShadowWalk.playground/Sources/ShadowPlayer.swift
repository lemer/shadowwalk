import Foundation
import SpriteKit


/// Represents a player in the game.
public class ShadowPlayer: SKNode, ShadowWatcher {
    /// static constants defining movement parameters
    private static let maxRunSpeed: CGFloat = 250
    private static let maxJumpForce: CGFloat = 400
    
    private var runDirection: CGFloat = 0.0
    private var jumpForce: CGFloat = 0.0

    /// shape of the player. This is a separate node instead of having
    /// ShadowPlayer inherit from SKShapeNode because it makes the switching
    /// animation easier to create
    public let radius: CGFloat
    private let shape: SKShapeNode
    
    /// This child node contains the physicsbody detecting proximity
    private let proximityNode: SKNode
    private static let proximityThreshold: CGFloat = 10
    
    /// This child node contains the physicsbody detecting contact with floor
    private let floorTouchNode: SKNode
    private static let floorTouchThreshold: CGFloat = 10
    
    /// Crop nodes used to animate the transition between dark and light modes
    private var srcCropNode: SKCropNode?
    private var dstCropNode: SKCropNode?

    private var canMove: Bool = true
    
    public var canSwitch: Bool {
        if !canMove {
            return false
        }
        for body in self.floorTouchNode.physicsBody?
            .allContactedBodies() ?? [] {
                if body.categoryBitMask.intersects(
                    with: PhysicsCategory.surface.mask) {
                    return true
                }
        }
        return false
    }
    
    public var touchesFloor: Bool {
        for body in self.floorTouchNode.physicsBody?
            .allContactedBodies() ?? [] {
                if body.categoryBitMask.intersects(
                    with: PhysicsCategory.anyOf(.surface, .platform)) {
                    return true
                }
        }
        return false
    }
    
    public init(radius: CGFloat, color: NSColor) {
        self.radius = radius
        self.proximityNode = SKNode()
        self.floorTouchNode = SKNode()
        self.shape = SKShapeNode(circleOfRadius: radius)
        
        super.init()
        
        // Setup shape
        self.shape.fillColor = color
        self.shape.strokeColor = color
        self.shape.zPosition = 100
        self.addChild(self.shape)
        
        // Setup physics
        self.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        self.physicsBody!.categoryBitMask = PhysicsCategory.player.mask
        self.physicsBody!.affectedByGravity = true
        self.physicsBody!.friction = 0.1
        self.physicsBody?.mass = 10
        self.physicsBody?.collisionBitMask =
            PhysicsCategory.anyOf(.surface, .platform)
        self.physicsBody?.contactTestBitMask =
            PhysicsCategory.anyOf(.killer, .platform, .surface)
        self.physicsBody?.allowsRotation = false
        
        self.proximityNode.physicsBody = SKPhysicsBody(
            circleOfRadius: radius + ShadowPlayer.proximityThreshold)
        self.proximityNode.physicsBody?.affectedByGravity = false
        self.proximityNode.physicsBody?.pinned = true
        self.proximityNode.physicsBody?.isDynamic = true
        self.proximityNode.physicsBody?.categoryBitMask =
            PhysicsCategory.playerProximity.mask
        self.proximityNode.physicsBody?.collisionBitMask =
            PhysicsCategory.nothing
        self.proximityNode.physicsBody?.contactTestBitMask =
            PhysicsCategory.anyOf(.surface, .goalProximity)
        self.addChild(self.proximityNode)
        
        self.floorTouchNode.physicsBody = SKPhysicsBody(
            circleOfRadius: radius - 1)
        self.floorTouchNode.physicsBody?.affectedByGravity = false
        self.floorTouchNode.physicsBody?.pinned = true
        self.floorTouchNode.physicsBody?.isDynamic = true
        self.floorTouchNode.physicsBody?.categoryBitMask =
            PhysicsCategory.playerProximity.mask
        self.floorTouchNode.physicsBody?.collisionBitMask =
            PhysicsCategory.nothing
        self.floorTouchNode.physicsBody?.contactTestBitMask =
            PhysicsCategory.anyOf(.surface, .platform)
        self.floorTouchNode.position =
            CGPoint(x: 0, y: -ShadowPlayer.floorTouchThreshold)
        self.addChild(self.floorTouchNode)
        
        self.setupAnimationCropNodes()
    }
    
    /// Initializes and sets up animation crop nodes. Those are used in the
    /// animation of the transition between dark and light mode
    private func setupAnimationCropNodes() {
        
        func addCropNode(maskY yPosition: CGFloat,
                               maskHeight: CGFloat) -> SKCropNode {
            let rect = CGRect(
                origin: CGPoint(x: -self.radius, y: yPosition),
                size: CGSize(width: self.radius * 2, height: maskHeight))
            let cropMask = SKShapeNode(rect: rect)
            cropMask.fillColor = .white
            
            let crop = SKCropNode()
            crop.maskNode = cropMask
            crop.zPosition = self.shape.zPosition
            
            self.addChild(crop)

            return crop
        }
        
        self.srcCropNode = addCropNode(maskY: -self.radius,
                                       maskHeight: 2 * self.radius)
        self.dstCropNode = addCropNode(maskY: -5 * self.radius,
                                       maskHeight: 4 * self.radius)
    }

    /// Plays the respawn animation for provided respawn position. The andThen
    /// function is called right after the player is teleported
    public func respawn(at pos: CGPoint, andThen: @escaping () -> ()) {
        self.removeAllActions()
        self.canMove = false
        self.physicsBody?.isDynamic = false
        self.physicsBody?.velocity = CGVector.zero

        let fadeDuration: TimeInterval = 0.4
        self.run(.sequence([
            .group([.fadeOut(withDuration: fadeDuration),
                    .scale(to: 1.5, duration: fadeDuration)]),
            .move(to: pos, duration: 0),
            .run(andThen),
            .group([.fadeIn(withDuration: fadeDuration),
                    .scale(to: 1.0, duration: fadeDuration)])
            ])) {
                self.canMove = true
                self.physicsBody?.isDynamic = true
        }
    }
    
    /// Saves provided movement instructions to be applied during the update
    /// phase
    public func move(towards dir: Direction) {
        switch dir {
        case .E, .W:
            self.runDirection = (dir == .E ? 1 : -1) * ShadowPlayer.maxRunSpeed
        case .N, .S:
            if self.touchesFloor {
                self.jumpForce = (dir == .N ? 1 : -1) * ShadowPlayer.maxRunSpeed
            }
        }
    }
    
    /// Updates the player's position based on current state of movement
    /// instructions
    public func update() {
        var dx: CGFloat = 0
        var dy: CGFloat = 0
        if self.canMove {
            if let v = self.physicsBody?.velocity {
                dy = v.dy
                if (self.runDirection != 0) {
                    dx = v.dx + (self.runDirection - v.dx)/10
                    dx = dx.clamped(ShadowPlayer.maxRunSpeed)
                } else {
                    dx = v.dx * 0.85
                }
                
                if (self.jumpForce != 0) {
                    dy = (v.dy + self.jumpForce)
                        .clamped(ShadowPlayer.maxJumpForce)
                    self.jumpForce *= 0.7
                }
            }
            self.physicsBody!.velocity = CGVector(dx: dx, dy: dy)
        }
    }

    /// Instantly stops movement along the provided direction
    public func stopMoving(along dir: Direction) {
        switch dir {
        case .E, .W:
            self.runDirection = 0
        case .N, .S:
            self.jumpForce = 0
        }
    }
    
    /// Switches to the provided mode, with or without animation.
    private func switchTo(mode: ShadowMode, animated: Bool) {
        // Decide next color and position based on next mode
        let nextColor: NSColor = mode == .soonDark ? .white : .black
        let deltaPosition = CGPoint(x: 0, y: 3 * self.radius)
        let nextPosition: CGPoint
        if mode == .soonDark {
            nextPosition = self.position - deltaPosition
        } else {
            nextPosition = self.position + deltaPosition
        }

        /// Helper function: code to be run once the transition is complete
        func finally() {
            self.physicsBody?.isDynamic = true
            self.position = nextPosition
            self.zRotation += CGFloat.pi
            self.shape.fillColor = nextColor
            self.shape.strokeColor = nextColor
            self.shape.alpha = 1
            
            if animated {
                ShadowScene.singleton?.shadowController.switchToNext()
            }
        }
        
        if animated {
            // Create two clones of the shape; one identical to the player's
            // shape, the other with the opposite color. Both are placed in
            // their corresponding crop node, and then identically moved
            // towards the post-animation position of the player. The crop nodes
            // are what makes this effect of the ball changing to and from black
            // as it crosses the surface of the platform it is sitting on.
            let srcShape = self.shape.copy() as! SKShapeNode
            srcShape.position = CGPoint.zero
            let dstShape = srcShape.copy() as! SKShapeNode
            dstShape.fillColor = nextColor
            dstShape.strokeColor = nextColor
            
            self.srcCropNode?.addChild(srcShape)
            self.dstCropNode?.addChild(dstShape)

            self.shape.alpha = 0
            
            let moveAction: SKAction =
                .move(by: CGVector(dx: 0, dy: -self.radius * 3), duration: 0.2)
            srcShape.run(.sequence([moveAction, .removeFromParent()]))
            dstShape.run(.sequence([moveAction, .removeFromParent()]),
                         completion: finally)
        } else {
            finally()
        }
    }

    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        var animate = true
        
        switch (src, dst) {
        case (.soonDark, .dark), (.soonLight, .light):
            return
        case (.light, .dark), (.dark, .light):
            // If the transition occurs instantly, no animation must take place
            animate = false
        default: ()
        }
        
        self.physicsBody?.isDynamic = false

        switchTo(mode: dst, animated: animate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

