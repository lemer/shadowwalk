import Foundation
import SpriteKit

/// Represents the light that follows the player around.
public class ShadowLight: SKLightNode, ShadowWatcher {
    public static let lightCategory: UInt32 = 1 << 10
    
    /// Looping animation of the light "breathing"
    private var breathingAction: SKAction {
        let duration: TimeInterval = 3
        return SKAction.repeatForever(
            SKAction.customAction(withDuration: duration) { (node, time) in
                let ratio = CGFloat(TimeInterval(time) / duration)
                let alpha = 2 * CGFloat.pi * ratio
                let fraction = 0.075 - 0.075 * cos(alpha)
                (node as? SKLightNode)?.lightColor =
                    self.baseLightColor.blended(
                        withFraction: fraction,
                        of: .clear) ?? self.baseLightColor
            }
        )
    }

    private var baseLightColor: NSColor =
        NSColor(white: 0.28, alpha: 1)
    private let baseShadowColor: NSColor =
        NSColor(white: 0, alpha: 0.75)
    
    /// SKNode this light will try to move towards
    private var target: SKNode?
    
    public var canMove: Bool = true
    
    public override init() {
        super.init()
        
        self.categoryBitMask = ShadowLight.lightCategory
        self.zPosition = ZLayer.light
        
        // light settings
        self.falloff = 0
        self.ambientColor = NSColor(white: 0.6, alpha: 1)
        self.shadowColor = NSColor.black.withAlphaComponent(0.75)
        self.lightColor = .green
        self.isEnabled = true
        
        // Physicsbody settingns
        self.physicsBody = SKPhysicsBody(circleOfRadius: 15)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.light.mask
        self.physicsBody?.collisionBitMask =
            PhysicsCategory.anyOf(.surface, .platform)
        self.physicsBody?.contactTestBitMask =
            PhysicsCategory.anyOf(.goalProximity)
        
        let circle = SKShapeNode(circleOfRadius: 3)
        circle.fillColor = .white
        circle.strokeColor = .white
        self.addChild(circle)
        
        self.run(self.breathingAction)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Instantly changes the color of the light
    public func setColor(_ color: NSColor) {
        self.lightColor = color
        self.baseLightColor = color
    }
    
    /// Changes node towards which the light is moving
    public func setTarget(_ node: SKNode) {
        self.target = node
    }
    
    /// Instantly moves the light to the provided position
    public func teleport(to position: CGPoint) {
        self.run(.move(to: position, duration: 0))
    }
    
    /// Executes one movement step of the light towards its target
    public func move() {
        if let t = self.target, self.canMove {
            self.position =
                self.position + (t.position - self.position).scaled(by: 0.1)
        }
    }
    
    /// Runs the animation of the light in which it flashes white with
    /// pure black shadows. Upon completion of the animation, it calls andThen
    public func flash(andThen: @escaping () -> ()) {
        let lightColor = self.lightColor
        self.removeAllActions()
        self.run(
            .customAction(withDuration: 1.0, actionBlock: { (node, time) in
                let light = node as! ShadowLight
                let frac = time / 1.0
                light.lightColor = lightColor.blended(
                    withFraction: frac, of: .white) ?? .white
                light.shadowColor = self.baseShadowColor.blended(
                    withFraction: frac, of: .black) ?? .black
            }),
            completion: andThen)
    }
    
    /// Lets the light flash back to the provided color. This also updates the
    /// light's base color, and resumes its breathing animation
    public func flashBack(withColor color: NSColor) {
        self.baseLightColor = color
        self.run(.sequence([
            .customAction(withDuration: 2.0, actionBlock: { (node, time) in
                let light = node as! ShadowLight
                let frac = time / 2.0
                light.lightColor = NSColor.white.blended(
                    withFraction: frac,
                    of: self.baseLightColor) ?? self.baseLightColor
                light.shadowColor = NSColor.black.blended(
                    withFraction: frac,
                    of: self.baseShadowColor) ?? self.baseShadowColor
            }),
            self.breathingAction
        ]))
    }
    
    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        switch dst {
        case .soonDark:
            self.canMove = false
            self.physicsBody?.isDynamic = false
        case .light:
            self.canMove = true
            self.physicsBody?.isDynamic = true
        default: ()
        }
    }

}
