import Foundation
import SpriteKit

/// Represents a goal in the game
public class ShadowGoal: SKShapeNode, ShadowWatcher {
    private let shape: SKShapeNode
    private let originalColor: NSColor
    private let breathingAction: SKAction

    public static let radius: CGFloat = 10
    public static let proximityRadius: CGFloat = 30
    
    /// Whether this goal can be triggered by contact with the player.
    public var isTriggerable: Bool {
        get {
            return (self.physicsBody?.categoryBitMask ?? 0)
                .intersects(with: PhysicsCategory.goalProximity.mask)
        }
        set {
            if newValue == true {
                self.physicsBody?.categoryBitMask
                    |= PhysicsCategory.goalProximity.mask
            } else if self.isTriggerable {
                self.physicsBody?.categoryBitMask
                    -= PhysicsCategory.goalProximity.mask
            }
        }
    }
    
    public init(color: NSColor) {
        self.shape = SKShapeNode(circleOfRadius: ShadowGoal.radius)
        self.originalColor = color
        
        // Setting up "breathing" animation
        let actionDuration: TimeInterval = 3
        self.breathingAction = SKAction.repeatForever(.customAction(
            withDuration: actionDuration,
            actionBlock: { (node, time) in
                let ratio = time / CGFloat(actionDuration)
                let alpha = sin( 2 * CGFloat.pi * ratio)
                node.setScale(1 + 0.2 * alpha)
        }))
        
        super.init()
        
        // Creating the shape. It is separate from this node in order to be
        // able to animate the scale of it without affecting the goal's
        // physicsbody
        self.shape.fillColor = NSColor.clear
        self.shape.strokeColor = color
        self.shape.lineWidth = 5
        self.shape.run(self.breathingAction)
        self.addChild(self.shape)
        
        self.zPosition = ZLayer.goals
        
        // Setting up proximity sensor
        self.physicsBody = 
            SKPhysicsBody(circleOfRadius: ShadowGoal.proximityRadius)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.pinned = true
        self.physicsBody?.categoryBitMask = PhysicsCategory.goalProximity.mask
        self.physicsBody?.collisionBitMask = PhysicsCategory.nothing
        self.physicsBody?.contactTestBitMask =
            PhysicsCategory.anyOf(.player, .light)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func didChangeShadows(from src: ShadowMode, to dst: ShadowMode) {
        let switchDuration: TimeInterval = 0.7
        switch (src, dst) {
        case (_, .soonDark), (.light, .dark) :
            // when about to go dark, use SKAction extension to scale down
            // and loose its color
            self.shape.removeAllActions()
            self.shape.run(
                .group([.scale(to: 0.8, duration: switchDuration),
                        .colorizeStroke(from: self.shape.strokeColor,
                                        to: .init(white: 0.8, alpha: 1),
                                        duration: switchDuration)
                ]))
        case (_, .soonLight), (.dark, .light):
            // Scale back up, regain color and resume breathing action
            self.shape.removeAllActions()
            self.shape.run(.sequence([
                .group([.scale(to: 1, duration: switchDuration),
                        .colorizeStroke(from: self.shape.strokeColor,
                                        to: self.originalColor,
                                        duration: switchDuration)]),
                self.breathingAction
                ]))
        default:
            ()
        }
    }
}

