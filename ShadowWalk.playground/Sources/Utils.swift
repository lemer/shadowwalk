import Foundation
import SpriteKit

/**
 This file contains most small classes, structs and extensions that I used to
 make the rest of the project easier to read or to implement.
 */

/// Helper class I used to evaluate performances of different parts of my code
/// This was very useful to detect which parts of my generatePhysics function
/// in ShadowScene were taking most computing time, so that I knew what to
/// optimize first. I optained a speedup of about 12 for the generatePhysics
/// using insights provided by this class.
public class Performance {
    private static var startTime: CFAbsoluteTime = 0
    private static var name: String = "_"
    
    public static func start(_ name: String = "Unknown") {
        self.name = name
        Performance.startTime = CFAbsoluteTimeGetCurrent()
        print("  %  Timer starts [\(name)]...")
    }
    public static func get() {
        let time = CFAbsoluteTimeGetCurrent()
        print("  %  Time elapsed [\(name)]: \(time - Performance.startTime)")
    }
    public static func getAndRestart(_ name: String = "Unknown") {
        Performance.get()
        Performance.start(name)
    }
}

public enum Direction {
    case N, E, S, W
}

/// Represents a point in spherical coordinates
public struct SphericPoint {
    public let rho: CGFloat
    public let phi: CGFloat
    
    public func rotated(by angle: CGFloat, truncating: Bool = true)
        -> SphericPoint {
            let newPhi = (self.phi + angle).truncatingRemainder(
                dividingBy: 2 * CGFloat.pi)
            return SphericPoint(rho: self.rho, phi: newPhi)
    }
}

public struct ZLayer {
    public static let background: CGFloat = 0
    public static let blocks: CGFloat = 10
    public static let light: CGFloat = 11
    public static let player: CGFloat = 30
    public static let goals: CGFloat = 40
    public static let surfaces: CGFloat = 50
    public static let ui: CGFloat = 100
}

public enum PhysicsCategory: Int {
    case surface = 0
    case platform
    case player
    case playerProximity
    case light
    case goalProximity
    case sensor
    case killer
    
    public static let nothing: UInt32 = 0
    
    public var mask: UInt32 {
        return 1 << (self.rawValue)
    }
    
    public static func anyOf(_ categories: PhysicsCategory...) -> UInt32 {
        var sum: UInt32 = 0
        for c in categories {
            sum += c.mask
        }
        return sum
    }
}

extension CGFloat {
    static public let floatingPrecision: CGFloat = 1e-6
    
    public func radLessThan(other: CGFloat) -> Bool {
        return (other - CGFloat.pi) <= self
            && self <= other
    }
    
    public func clamped(_ within: CGFloat) -> CGFloat {
        return Swift.min(within, Swift.max(-within, self))
    }
}

extension CGPath {
    public static func from(points: [CGPoint]) -> CGPath? {
        let path = CGMutablePath()
        if points.isEmpty {
            return nil
        }
        path.move(to: points[0])
        for point in points.dropFirst() {
            path.addLine(to: point)
        }
        path.closeSubpath()
        return path.copy()
    }
}

extension CGVector {
    public static func +(left: CGVector, right: CGVector) -> CGVector {
        return CGVector(dx: left.dx + right.dx,
                        dy: left.dy + right.dy)
    }
    public static func *(left: CGFloat, right: CGVector) -> CGVector {
        return CGVector(dx: left * right.dx,
                        dy: left * right.dy)
    }
    public func magnitude() -> CGFloat {
        return sqrt(self.dx * self.dx + self.dy * self.dy)
    }
    public func normalized() -> CGVector {
        let m = self.magnitude()
        if m == 0 {
            return self
        }
        return CGVector(dx: self.dx / m, dy: self.dy / m)
    }
}

extension CGPoint {
    public static func -(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x - right.x,
                       y: left.y - right.y)
    }
    public static func +(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x,
                       y: left.y + right.y)
    }
    public func scaled(by a: CGFloat) -> CGPoint {
        return CGPoint(x: self.x * a, y: self.y * a)
    }
    public func toSpheric() -> SphericPoint {
        let rot = CGPoint.zero.angle(with: self)
        return SphericPoint(rho: sqrt(self.x * self.x + self.y * self.y),
                            phi: rot)
    }
    public func angle(with other: CGPoint) -> CGFloat {
        let dx = other.x - self.x
        let dy = other.y - self.y
        var rot: CGFloat
        if dx == 0 {
            rot = dy < 0 ? CGFloat.pi * 1.5 : CGFloat.pi * 0.5
        } else {
            rot = atan(dy / dx)
            if dx <= 0 {
                rot += CGFloat.pi
            } else if dy <= 0 {
                rot += CGFloat.pi * 2
            }
        }
        return rot
    }
    
    public func distanceTo(_ other: CGPoint) -> CGFloat {
        let diff = self - other
        return sqrt(diff.x * diff.x + diff.y * diff.y)
    }
}

extension UInt32 {
    public func intersects(with other: UInt32) -> Bool {
        return (self & other) != 0
    }
}

extension SKPhysicsContact {
    public func isBetween(_ a: UInt32, and b: UInt32) -> Bool {
        let bA = self.bodyA
        let bB = self.bodyB
        
        return ((bA.categoryBitMask & a != 0
            && bB.categoryBitMask & b != 0)
            || (bB.categoryBitMask & a != 0
                && bA.categoryBitMask & b != 0))
    }
}

extension SKAction {
    public static func colorizeStroke(from src: NSColor,
                                      to dst: NSColor,
                                      duration: TimeInterval) -> SKAction {
        return SKAction.customAction(
            withDuration: duration,
            actionBlock: { (node, time) in
                if let shape = node as? SKShapeNode {
                    let fraction = CGFloat(TimeInterval(time) / duration)
                    shape.strokeColor =
                        src.blended(withFraction: fraction, of: dst) ?? src
                }
        })
    }
}
