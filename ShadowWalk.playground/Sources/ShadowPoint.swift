import Foundation

/**
 Describes a visible point during the computation of shadow surfaces. For more
 information about shadow surface computation, see the generatePhysics
 function in ShadowScene.
 
 When computing the shadow surfaces, the concept of a platform is ignored and
 we use ShadowPoints instead. Each platform is mapped to the two or three
 points that are visible from the light's position. Additionally, each such
 point remembers all relevent information about its original platform: which of
 the two or three points it is (that is, an extremity, or one in the middle);
 and in which cardinal direction the next visible point is, if any.
 
 More precisely, this class stores the following information:
   - cartesian and spherical coordinates of the point with respect to the light
 source's position
   - order of the point: this corresponds to the relative ordering of this point
 with respect to other points generated from the same platform. Each platform
 thus has at exactly one .first and one .last point. If a platform has three
 visible points, the middle one has order .middle. Additionally, during
 generation of shadow surfaces, new points are created at intersections between
 platforms. Those points have a special order value: .intersection.
   - direction to the next point. This represents the cardinal direction
 pointing to the next visible point of the original platform. If this point is 
 the last visible point, its direction is nil. This allows a ShadowPoint to
 also store the information about the line between itself and the next visible
 point.

 We say that a point A is a source of a destination point B if A-B forms an
 edge in the original platform.
 
 */
public class ShadowPoint : Hashable {
    public enum Order: Int {
        case first = 0, middle, last, intersection
    }
    
    /// Position of this point in cartesian coordinates
    public let point: CGPoint
    /// Position of this point in spherical coordinates
    public let spheric: SphericPoint
    /// Direction pointing towards next visible point in the shape, or nil
    /// if last visible point
    public let direction: Direction?
    /// Order of this point among other visible points in the shape
    public let order: Order
    
    private var x: CGFloat { return self.point.x }
    private var y: CGFloat { return self.point.y }
    
    public init(point: CGPoint,
                spheric: SphericPoint,
                nextPoint: CGPoint?,
                order: Order) {
        self.point = point
        self.spheric = spheric
        self.order = order
        
        if let nextPoint = nextPoint {
            var dir = Direction.E
            let angle = point.angle(with: nextPoint)

            if angle > CGFloat.pi / 4 {
                if angle < CGFloat.pi * 3 / 4 {
                    dir = .N
                } else if angle < CGFloat.pi * 5 / 4 {
                    dir = .W
                } else if angle < CGFloat.pi * 7 / 4 {
                    dir = .S
                }
            }
            self.direction = dir
        } else {
            self.direction = nil
        }
    }

    /**
     Checks whether the line between this point and the next visible point
     intersects the provided point. We then call this point a source of the
     provided point.
     */
    public func isSrcOf(dst: CGPoint) -> Bool {
        guard let dir = self.direction else {
            return false
        }
        
        switch dir {
        case .E, .W :
            if (dst.y == self.point.y) {
                return (dir == .E && dst.x >= self.point.x
                    || dir == .W && dst.x <= self.point.x)
            }
        case .N, .S :
            if (dst.x == self.point.x) {
                return (dir == .N && dst.y >= self.point.y
                    || dir == .S && dst.y <= self.point.y)
            }
        }
        return false
    }
    
    /**
     Returns the intersection between two lines :
       - the line between this point and the next visible point
       - the line defined by the two provided points (from, to)
     When set, the infinite parameter allows the function to look for
     intersection points on the ray leaving 'from' rather than the segment.
     */
    public func getIntersectionWith(lineFrom from: CGPoint,
                                    to: CGPoint,
                                    infinite: Bool = false) -> CGPoint? {
        guard let dir = self.direction else {
            return nil
        }
        
        var a: CGFloat? = nil
        var b: CGFloat? = nil
        if (from.x != to.x) {
            a = (to.y - from.y) / (to.x - from.x)
            b = from.y - a! * from.x
        }
        
        switch dir {
        case .E, .W:
            // check for the right y interval
            if (( !infinite
                && (self.y <= to.y || self.y <= from.y)
                && (self.y >= to.y || self.y >= from.y))
                || (infinite
                    && ((from.y < to.y && from.y < self.y)
                        || (from.y >= to.y && from.y >= self.y)))) {
                // check that self is on the correct side of the line
                if (dir == .E) {
                    if a == nil {
                        if (self.x <= from.x) {
                            return CGPoint(x: from.x, y: self.y)
                        }
                    } else if a == 0 {
                        if (self.x <= from.x || self.x <= to.x) {
                            return CGPoint(x: max(from.x, to.x), y: self.y)
                        }
                    } else {
                        let interX = (self.y - b!)/a!
                        if interX >= self.x {
                            return CGPoint(x: interX, y: self.y)
                        }
                    }
                }
                if (dir == .W) {
                    if a == nil {
                        if (self.x >= from.x) {
                            return CGPoint(x: from.x, y: self.y)
                        }
                    } else if a == 0 {
                        if (self.x >= from.x || self.x >= to.x) {
                            return CGPoint(x: min(from.x, to.x), y: self.y)
                        }
                    } else {
                        let interX = (self.y - b!)/a!
                        if interX <= self.x {
                            return CGPoint(x: interX, y: self.y)
                        }
                    }
                }
            }
            return nil
        case .N, .S:
            if ((!infinite
                && (self.x <= to.x || self.x <= from.x)
                && (self.x >= to.x || self.x >= from.x))
                || (infinite
                    && ((from.x <= to.x && from.x <= self.x)
                        || (from.x >= to.x && from.x >= self.x)))) {
                if (dir == .N) {
                    if a == nil {
                        if (self.y <= from.y || self.y <= to.y) {
                            return CGPoint(x: self.x, y: max(from.y, to.y))
                        }
                    } else {
                        let interY = a! * self.x + b!
                        if interY >= self.y {
                            return CGPoint(x: self.x, y: interY)
                        }
                    }
                }
                if (dir == .S) {
                    if a == nil {
                        if (self.y >= from.y || self.y >= to.y) {
                            return CGPoint(x: self.x, y: min(from.y, to.y))
                        }
                    } else {
                        let interY = a! * self.x + b!
                        if interY <= self.y {
                            return CGPoint(x: self.x, y: interY)
                        }
                    }
                }
            }
        }
        
        return nil
    }
    
    /**
     Tries to find the closest intersection between a line generated by a
     source in srcs, and the segment defined by the pair (src, dst).
     When set to true, the infinite parameter allows the function to look for
     intersection points on the ray leaving 'from' rather than the segment only.
     */
    public static func findClosestIntersectingPoint(
        in srcs: Set<ShadowPoint>,
        between src: CGPoint,
        and dst: CGPoint,
        infinite: Bool = false)
        -> CGPoint? {
            
            var interSrc2 = [(ShadowPoint, CGPoint)]()
            for src2 in srcs {
                // if the line src2-.. intersects our line, consider it
                if let inter = src2.getIntersectionWith(lineFrom: src,
                                                        to: dst,
                                                        infinite: infinite) {
                    interSrc2.append((src2, inter))
                }
            }
            // if we have found intersections, we return the closest one
            if let (_, inter) = interSrc2.min(by: 
                {$0.1.distanceTo(CGPoint.zero) < $1.1.distanceTo(CGPoint.zero)}
                ) {
                return inter
            }
            
            return nil
    }

    public static func == (lhs: ShadowPoint, rhs: ShadowPoint) -> Bool {
        return lhs.point == rhs.point
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.point.x)
        hasher.combine(self.point.y)
    }
}
