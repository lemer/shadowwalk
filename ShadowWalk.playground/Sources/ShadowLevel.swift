import Foundation
import SpriteKit

/**
 Represents a level in the game.
 
 It extracts information from the node provided to its initializer. This node
 must be a container containing as children all nodes describing the level.
 It is meant to be used with a container node found in an .sks file.
 
 Each child node is evaluated based on its name :
   - "platform" is the basic platform with the white, tengible surface on top
   - "block" is similar to platform, without the top white surface
   - "killer" is a platform which has a red, killing top surface
   - "glass" is a block that does not cast shadows
   - "goal" is used to encode the position and color of the goal
   - "player" is used to encode the starting position of the player
   - "label" is text visible in the level
 
 In addition, each node can have additional information encoded in its userData
 attribute, which is used to encode moving blocks, or disapearing text for
 example. See ShadowPlatform.swift for more details.
 
 */
public class ShadowLevel: SKNode {
    public private(set) var playerPosition: CGPoint!
    public private(set) var goal: ShadowGoal!
    public private(set) var color: NSColor!
    public private(set) var teleportOnLoad: Bool = false
    public let platforms: [Platform]
    public let shadowPlatforms: [ShadowPlatform]
    public let labels: [ShadowLabel]
    public let movingPlatforms: [Platform]
    
    /**
     Extracts information about a level from the provided container node.
     
     See the class description, ShadowPlatform.swift, and TemplateLevel.sks
     for more details on level creation!
     */
    public init?(from container: SKNode) {
        var labels = [ShadowLabel]()
        var platforms = [Platform]()
        var shadowPlatforms = [ShadowPlatform]()
        var moving = [Platform]()
        
        for node in container.children {
            node.removeFromParent()
            var platform: Platform! = nil
            
            if let spritenode = node as? SKSpriteNode {
                switch node.name {
                case "glass":
                    platform = Platform(from: spritenode)
                case "platform":
                    platform = ShadowPlatform(from: spritenode)
                    shadowPlatforms.append(platform as! ShadowPlatform)
                case "block":
                    platform = ShadowPlatform(from: spritenode)
                    (platform as! ShadowPlatform).removeSurface()
                    shadowPlatforms.append(platform as! ShadowPlatform)
                case "killer":
                    platform = KillerPlatform(from: spritenode)
                    shadowPlatforms.append(platform as! ShadowPlatform)
                case "goal":
                    self.goal = ShadowGoal(color: spritenode.color)
                    self.goal.position = spritenode.position
                    self.color = spritenode.color
                case "player":
                    self.playerPosition = node.position
                    self.teleportOnLoad =
                        (node.userData?["teleport"] as? Bool) ?? false
                default:
                    continue
                }
            } else if let label = node as? SKLabelNode, node.name == "label" {
                labels.append(ShadowLabel(from: label))
            } else {
                continue
            }
            
            if platform != nil {
                platforms.append(platform)
                
                if platform.isMoving {
                    moving.append(platform)
                }
            }
        }

        self.platforms = platforms
        self.shadowPlatforms = shadowPlatforms
        self.labels = labels
        self.movingPlatforms = moving
        
        super.init()
        
        // Adding all elements to this level now that it is initialized
        for label in labels {
            self.addChild(label)
        }
        for platform in self.platforms {
            self.addChild(platform)
        }
        self.addChild(self.goal)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func startWatching(controller: ShadowController) {
        self.goal.startWatching(controller: controller)
        for p in self.platforms {
            p.startWatching(controller: controller)
        }
        for l in self.labels {
            l.startWatching(controller: controller)
        }
    }
}
